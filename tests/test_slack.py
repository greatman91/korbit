import unittest
from pusher.slack import PushSlack

class TestSlacke(unittest.TestCase):

    def setUp(self):
        self.pusher = PushSlack()

    def test_send_message(self):
        self.pusher.send_message(message= "This is the msg")

    def tearDown(self):
        pass

if __name__ == "__main__":
    unittest.main()