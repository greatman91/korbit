from abc import ABC, abstractmethod
import datetime

class Strategy(ABC):
    @abstractmethod
    def run(self):
        pass

    def update_trade_status(self, db_handler=None, buy=None, value=None):
        if db_handler is None or buy is None or value is None:
            raise Exception("Need to buy value or status")
        db_handler.set_db("trader","trade_status")
        db_handler.update_item(buy, {"$set":value})

    def order_buy_transaction(self, machine=None, db_handler=None, coin_type=None,  order_type="limit"):
        if coin_type is None:
            raise Exception("Need to param")
        db_handler.set_db("trader","trade_status")
        krws=int(machine.get_wallet_status()['krw']['avail'])
        orders = machine.get_orderbook(coin_type)
        favor_price = int(orders['asks'][3][0])
        favor_vol = round(krws/favor_price, 8)

        result = machine.buy_coin_order(coin_type=coin_type,
                                        price=str(favor_price),
                                        qty=str(favor_vol),#str(self.BUY_COUNT),
                                        order_type=order_type)

        if result["status"] == "success":
            db_handler.insert_item({"status":"BUY_ORDERED",
                                    "coin":coin_type,
                                    "order_id":str(result["orderId"]),
                                    "amount":favor_vol,
                                    "price":machine.get_my_order_status(coin_type=coin_type,order_id=str(result["orderId"]))[0]['price'],
                                    "order_time":int(datetime.datetime.now().timestamp()),
                                    "transaction_status":"success",
                                    "machine":str(machine)})
            return result["orderId"]
        else:
            return None

    def order_sell_transaction(self, machine=None, db_handler=None, coin_type=None, order_type="limit"):
        if coin_type is None:
            raise Exception("Need to param")
        db_handler.set_db("trader","trade_status")
        coin = coin_type[:3]
        coins=float(machine.get_wallet_status()[coin]['avail'])
        orders = machine.get_orderbook(coin_type)
        favor_price = orders['bids'][1][0]

        result = machine.sell_coin_order(coin_type=coin_type,
                                        price=str(favor_price),
                                        qty=str(round(coins,8)),
                                        order_type=order_type)
        if result["status"] == "success":
            db_handler.insert_item({"status":"SELL_ORDERED",
                                    "coin":coin_type,
                                    "order_id":str(result["orderId"]),
                                    "amount":round(coins,8),
                                    "price":machine.get_my_order_status(coin_type=coin_type,order_id=str(result["orderId"]))[0]['price'],
                                    "order_time":int(datetime.datetime.now().timestamp()),
                                    "transaction_status":"success",
                                    "machine":str(machine)})
            return result["orderId"]
        else:
            return None

    def order_cancel_transaction(self, machine=None, db_handler=None, coin_type=None, item=None):
        db_handler.set_db("trader","trade_status")
        if coin_type is None or item is None:
            raise Exception("Need to param")
        if item["status"] == "BUY_ORDERED":
            result = machine.cancel_coin_order(coin_type=coin_type, order_id=item["buy_order_id"])
            if result[0]["status"] == "success":
                db_handler.update_item({"_id":item["_id"]}, {"$set":{"status":"CANCEL_ORDERED", "cancel_order_time":int(datetime.datetime.now().timestamp()),
                                "error":"success"}})
                return item["buy_order_id"] 
            else:
                return None
        elif item["status"] == "SELL_ORDERED":
            result = machine.cancel_coin_order(coin_type=coin_type, order_id=item["sell_order_id"])
            if result[0]["status"] == "success":
                db_handler.update_item({"_id":item["_id"]}, {"$set":{"status":"CANCEL_ORDERED", "cancel_order_time":int(datetime.datetime.now().timestamp()),
                                "error":"success"}})
                return item["sell_order_id"] 
            else:
                return None
