import os
import sys
project_dir = os.path.abspath(os.getcwd())
sys.path.append(project_dir)
import time

from strategy.base_strategy import Strategy
from machine.korbit_machine import KorbitMachine
from machine.coinone_machine import CoinOneMachine
from db.mongodb.mongodb_handler import MongoDbHandler
from pusher.slack import PushSlack
import configparser, datetime, traceback, sys
from logger import get_logger
import redis
from pandas import DataFrame
import pandas as pd
import json

logger = get_logger("step_trade")

class StepTrade(Strategy):
     
    def __init__(self, machine=None, db_handler=None, coin_type=None, pusher=None):
        if machine is None or db_handler is None:
            raise Exception("Need to machine, db, coin type, strategy")
        if isinstance(machine, KorbitMachine):
            logger.info("Korbit machine")
            self.coin_type="btc_krw"#coin_type+"_krw"
        elif isinstance(machine, CoinOneMachine):
            logger.info("CoinOne machine")
        self.coin = self.coin_type
        self.machine = machine
        self.pusher = pusher
        self.db_handler = db_handler

        self.redis = redis.StrictRedis(host='localhost', port=6379, db=0)
        self.token = self.redis.get(str(self.machine)+self.machine.get_username())

        if self.token == None:
            logger.info("set new token")
            saved_refresh_token = self.redis.get(str(self.machine)+self.machine.get_username()+"refresh")
            if saved_refresh_token is None:
                expire, token, refresh = self.machine.set_token(grant_type="password")
            else:
                self.machine.refresh_token = saved_refresh_token.decode("utf-8")
                expire, token, refresh = self.machine.set_token(grant_type="refresh_token")
            self.redis.set(str(self.machine)+self.machine.get_username(), token, expire-600)
            self.redis.set(str(self.machine)+self.machine.get_username()+"refresh", refresh, 2400)
            self.token = token
        else:
            self.token = self.token.decode("utf-8")
            self.machine.access_token = self.token
 #       expire, token,refresh=self.machine.set_token(grant_type="password")
 #       self.redis.set(str(self.machine)+self.machine.get_username(), token, expire-600)
 #       self.redis.set(str(self.machine)+self.machine.get_username()+"refresh", refresh, 2400)
 #       print("token : ",self.token)
        logger.info(self.token)
        logger.info(self.coin_type)
        self.wallet_status = self.machine.get_wallet_status()


    def set_token(self):
        self.token = self.redis.get(str(self.machine)+self.machine.get_username())
        if self.token == None:
            logger.info("set new token")
            saved_refresh_token = self.redis.get(str(self.machine)+self.machine.get_username()+"refresh")
            if saved_refresh_token is None:
                expire, token, refresh = self.machine.set_token(grant_type="password")
            else:
                self.machine.refresh_token = saved_refresh_token.decode("utf-8")
                expire, token, refresh = self.machine.set_token(grant_type="refresh_token")
            self.redis.set(str(self.machine)+self.machine.get_username(), token, expire-600)
            self.redis.set(str(self.machine)+self.machine.get_username()+"refresh", refresh, 2400)
            self.token = token
        else:
            self.token = self.token.decode("utf-8")
            self.machine.access_token = self.token

    def cci(self):
#        self.db_handler(collection_name='candle')
        now = datetime.datetime.now()
        one_day_ago = now - datetime.timedelta(minutes=3600)
        one_day_ago_timestamp = int(one_day_ago.timestamp())

        yesterday=now.day-1
        pipeline = [{"$match":{"ts": {"$gte": one_day_ago_timestamp}, "coin":self.coin_type}}
                             ,{"$sort":{"_id":-1}},{"$limit":50},{"$sort":{"_id":1}}]
        query_result = self.db_handler.aggregate(pipeline,"coiner","candle")
        res = query_result._CommandCursor__data
        last_price = self.machine.get_ticker(self.coin_type)["last"]

        res[-1]['last'] = last_price
        if res[-1]['max_val']<res[-1]["last"]:
            res[-1]['max_val'] = last_price
        if res[-1]['min_val'] >res[-1]['last']:
            res[-1]['min_val'] = last_price
        res = list(res)
 
        frame = DataFrame(res)

        constant = 0.015
        n = 14
        hi = pd.to_numeric(frame['max_val'], errors='coerce')
        lo = pd.to_numeric(frame['min_val'], errors='coerce')
        cl = pd.to_numeric(frame['last'], errors='coerce')

        frame['TP'] = (hi + lo + cl) / 3 
        CCI = pd.Series((frame['TP'] - frame['TP'].rolling(window=n).mean()) / (constant * frame['TP'].rolling(window=n).std()), name = 'CCI_' + str(n)) 
        val = []
        val.append(CCI.iloc[-1])
        val.append(CCI.iloc[-1]-CCI.iloc[-2])
        val.append(CCI.iloc[-2])
        return val

    def buy_scenario(self):
        flag=1
        while(flag==1):
            now = datetime.datetime.now()
            print(now)
            coins = ['xrp','btc','etc','eth','bch','btg' ]
            for item in coins:
                self.coin_type=item+"_krw"
                cci_val = self.cci()
                print(self.coin_type," : ",cci_val)
                if cci_val[0]>100 and  cci_val[1]>0 and cci_val[1]<60 and cci_val[2]<100: 
                    print(self.coin_type)
                    flag = 0
                    break     

            print("                                                       ")       

            time.sleep(30)     

        if cci_val[0]>100 and  cci_val[1]>0 and cci_val[2]<100: 
            self.set_token()
            self.order_buy_transaction(machine=self.machine, db_handler=self.db_handler, coin_type=self.coin_type)
            send_msg ={"buy transaction made : coin":self.coin_type}
            self.pusher.send_message(message = send_msg)
            time.sleep(10)
            self.wallet_status = self.machine.get_wallet_status() 
        else:
            print("waiting for buying")
            print(cci_val)
            return
 
    def sell_scenario(self):
        cci_val = self.cci()

        if cci_val[0]<100 and  cci_val[1]<0 and cci_val[2]>100: #down stream
            self.set_token()
            self.order_sell_transaction(machine=self.machine, db_handler=self.db_handler, coin_type=self.coin_type)
            send_msg ={"sell transaction made : coin":self.coin_type}
            self.pusher.send_message(message = send_msg)
            time.sleep(10)
            self.wallet_status = self.machine.get_wallet_status() 
        else:
            print("waiting for selling")
            print(cci_val)
            return
        
            
    def run(self):
        msg = {"krw" : self.wallet_status['krw']['avail']}
        self.pusher.send_message(message = msg)
        while(True):
            if float(self.wallet_status["krw"]["avail"])>500:
                print("buy session")
                try:
                    self.buy_scenario()
                except Exception:
                    continue
            else:
                print("sell session")
                try:
                    self.sell_scenario()
                except Exception:
                    continue
            time.sleep(60)
  
if __name__ == "__main__":
    config = configparser.ConfigParser()
    config.read('conf/config.ini')
    client_id = config['KORBIT']['client_id']
    client_secret = config['KORBIT']['client_secret']
    username = config['KORBIT']['username']
    password = config['KORBIT']['password']
    mongodb = MongoDbHandler("local", "coiner", "price_info")

    korbit_machine = KorbitMachine(mode="Prod",client_id=client_id,
                                   client_secret=client_secret,
                                   username=username,
                                   password=password)
    pusher = PushSlack()
    if len(sys.argv) > 0:
        trader = StepTrade(machine=korbit_machine, db_handler=mongodb, coin_type=None, pusher=pusher)
        trader.run()
    
