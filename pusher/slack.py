import telegram
from pusher.base_pusher import Pusher
import configparser

class PushSlack(Pusher):

    def __init__(self):
        config = configparser.ConfigParser()
        config.read('conf/config.ini')
        token = config['TELEGRAM']['token']
        self.bot = telegram.Bot(token)
        self.chat_id = 495733797
    def send_message(self, chat_id=495733797, message=None):
        self.bot.sendMessage(chat_id, message)
