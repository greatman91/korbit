import os
import sys
project_dir = os.path.abspath(os.getcwd())
sys.path.append(project_dir)

from db.mongodb import mongodb_handler
import sys, traceback, configparser
from optparse import OptionParser
import datetime
from machine.korbit_machine import KorbitMachine
from scheduler.coiner import Coiner
from celery import Celery

app = Celery('candle', backend='redis://localhost:6379/0', broker='redis://localhost:6379/0')

app.conf.beat_schedule = {
    'add-every-15-min': {
        'task': 'scheduler.candle.get_fif_info',
        'schedule': 899.0,
        'args':()
    },
}


@app.task
def get_fif_info():
    now = datetime.datetime.now()
    one_min_ago = now - datetime.timedelta(minutes=15)
    one_min_ago_timestamp = int(one_min_ago.timestamp())
    pipeline = [
        {"$match": {"timestamp": {"$gte": one_min_ago_timestamp}}},
                {"$group": {"_id": "$coin",
                            "min_val": {"$min": "$price"},
                            "max_val": {"$max": "$price"},
                            "sum_val": {"$sum": "$amount"},
                            "last": {"$last": "$price"},
                            "year": {"$last": "$year"},
                            "month": {"$last": "$month"},
                            "day": {"$last": "$day"},
                            "hour": {"$last": "$hour"},
                            "min": {"$last": "$minute"}
                            }}]
    mongodb = mongodb_handler.MongoDbHandler("local", "coiner", "price_info")
    query_result = mongodb.aggregate(pipeline)
    for item in query_result._CommandCursor__data:
        item['coin'] = item.pop("_id")
    mongodb1 = mongodb_handler.MongoDbHandler("local", "coiner", "candle")
    ids1 = mongodb1.insert_items(query_result._CommandCursor__data)