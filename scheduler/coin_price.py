import os
import sys
project_dir = os.path.abspath(os.getcwd())
sys.path.append(project_dir)

from db.mongodb import mongodb_handler
import sys, traceback, configparser
from optparse import OptionParser
import datetime
from machine.korbit_machine import KorbitMachine
from scheduler.coiner import Coiner
from celery import Celery

app = Celery('get_coin_info', backend='redis://localhost:6379/0', broker='redis://localhost:6379/0')

app.conf.beat_schedule = {
    'add-every-1-min': {
        'task': 'scheduler.coin_price.get_coin_info',
        'schedule': 59.0,
        'args':()
    },
    'add-every-15-min': {
        'task': 'scheduler.coin_price.get_fif_info',
        'schedule': 899.0,
        'args':()
    }
    ,
}


@app.task
def get_coin_info():
    config = configparser.ConfigParser()
    config.read('conf/config.ini')
    client_id = config['KORBIT']['client_id']
    client_secret = config['KORBIT']['client_secret']
    username = config['KORBIT']['username']
    password = config['KORBIT']['password']
    korbit_machine = KorbitMachine(mode="Prod",client_id=client_id,
                                        client_secret=client_secret,
                                        username=username,
                                        password=password)
    coiner = Coiner(korbit_machine)
    result_etc = coiner.get_filled_orders(currency_pair="etc_krw")
    result_eth = coiner.get_filled_orders(currency_pair="eth_krw")
    result_btc = coiner.get_filled_orders(currency_pair="btc_krw")
    result_xrp = coiner.get_filled_orders(currency_pair="xrp_krw")
    result_bch = coiner.get_filled_orders(currency_pair="bch_krw")
    result_btg = coiner.get_filled_orders(currency_pair="btg_krw")
    mongodb = mongodb_handler.MongoDbHandler("local", "coiner", "price_info")
    result_list = result_etc + result_eth + result_btc + result_xrp + result_bch + result_btg
    ids = mongodb.insert_items(result_list)

@app.task
def get_fif_info():
    now = datetime.datetime.now()
    one_min_ago = now - datetime.timedelta(minutes=15)
    one_min_ago_timestamp = int(one_min_ago.timestamp())
    pipeline = [{"$match": {"timestamp": {"$gte": one_min_ago_timestamp}}},
                {"$group": {"_id": "$coin",
                            "min_val": {"$min": "$price"},
                            "max_val": {"$max": "$price"},
                            "sum_val": {"$sum": "$amount"},
                            "last": {"$last": "$price"},
                            "year": {"$last": "$year"},
                            "month": {"$last": "$month"},
                            "day": {"$last": "$day"},
                            "hour": {"$last": "$hour"},
                            "min": {"$last": "$minute"},
                            "ts":{"$last":"$timestamp"}
                            }}]
    mongodb = mongodb_handler.MongoDbHandler("local", "coiner", "price_info")
    query_result = mongodb.aggregate(pipeline)

    for item in query_result._CommandCursor__data:
        item['coin'] = item.pop("_id")
    mongodb1 = mongodb_handler.MongoDbHandler("local", "coiner", "candle")
    ids1 = mongodb1.insert_items(query_result._CommandCursor__data)
