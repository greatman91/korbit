import requests
import time
from machine.base_machine import Machine

class KorbitMachine(Machine):
    def __init__(self, mode="Test", client_id=None, client_secret=None, username=None, password=None):
        if client_id is None or client_secret is None or username is None or password is None:
            raise Exception("Need to client infomation.")
        self.host = "https://api.korbit.co.kr"
        self.client_id = client_id
        self.client_secret = client_secret
        self.username = username
        self.password = password
        self.access_token = None
        self.refresh_token = None
        self.token_type = None

    def get_username(self):
        return self.username

    def get_nonce(self):
        return str(int(time.time()))

    def get_token(self):
        if self.access_token is not None:
            return self.access_token
        else:
            raise Exception("Need to set_token")

    def set_token(self, grant_type="password"):
        """
        set token
        grant_type: password, refresh_token

        saved self.access_token, self.refresh_token, self.token_type
        """
        token_api_path = "/v1/oauth2/access_token"

        url_path = self.host + token_api_path
        if grant_type == "password":
            data = {"client_id":self.client_id,
                    "client_secret":self.client_secret,
                    "username":self.username,
                    "password":self.password,
                    "grant_type":grant_type}
        elif grant_type == "refresh_token":
            data = {"client_id":self.client_id,
                    "client_secret":self.client_secret,
                    "refresh_token":self.refresh_token,
                    "grant_type":grant_type}
        else:
            raise Exception("Unexpected grant_type")

        res = requests.post(url_path, data=data)
        result = res.json()
        self.access_token = result["access_token"]
        self.token_type = result["token_type"]
        self.refresh_token = result["refresh_token"]
        self.expire = result["expires_in"]
        return self.expire, self.access_token, self.refresh_token

    def get_ticker(self, coin_type=None):
        """
        Get Ticker

        args:
            coin_type

        Returns:
            timestamp
            last
            high
            low
            volume
        """
        if coin_type is None:
            raise Exception('Need to coin type') 
        time.sleep(1)
        payload = {'currency_pair':coin_type}
        ticker_api_path = "/v1/ticker/detailed"
        url_path = self.host + ticker_api_path
        res = requests.get(url_path, params=payload)
        response_json = res.json()
        result={}
        result["timestamp"] = str(response_json["timestamp"])
        result["last"] = response_json["last"]
        result["high"] = response_json["high"]
        result["low"] = response_json["low"]
        result["volume"] = response_json["volume"]
        return result
    
    def get_filled_orders(self, coin_type=None, per="minute"):
        """
        List of filled orders

        Keyword arguments:
        per -- last time minute,hour,day

        Returns:
            api response
        """
        if coin_type is None:
            raise Exception("Need to coin_type")
        time.sleep(1) 
        payload = {'currency_pair':coin_type,'time':per}
        orders_api_path = "/v1/transactions" 
        url_path = self.host + orders_api_path
        res = requests.get(url_path, params=payload)
        result = res.json()
        return result

    def get_constants(self):
        """
        Get constant values

        Keyword arguments:

        Returns:
            api response
        """
        time.sleep(1)
        constants_api_path = "/v1/constants"
        url_path = self.host + constants_api_path
        res = requests.get(url_path)
        result = res.json()
        self.constants = result
        return result

    def get_wallet_status(self):
        """
        Get wallet_status
        """
        time.sleep(1)
        wallet_status_api_path ="/v1/user/balances"
        url_path = self.host + wallet_status_api_path
        headers = {"Authorization":"Bearer " + self.access_token}
        res = requests.get(url_path, headers=headers)
        print(res)
        result = res.json()
        wallet_status = {"btc":{},"eth":{},"etc":{},"bch":{},"xrp":{},"btg":{},"krw":{}}
        wallet_status["btc"]["avail"] = result["btc"]["available"]
        wallet_status["eth"]["avail"] = result["eth"]["available"]
        wallet_status["etc"]["avail"] = result["etc"]["available"]
        wallet_status["bch"]["avail"] = result["bch"]["available"]
        wallet_status["xrp"]["avail"] = result["xrp"]["available"]
        wallet_status["btg"]["avail"] = result["btg"]["available"]
        wallet_status["krw"]["avail"] = result["krw"]["available"]

        wallet_status["btc"]["balance"] = str(float(result["btc"]["trade_in_use"]) + float(result["btc"]["withdrawal_in_use"]))
        wallet_status["eth"]["balance"] = str(float(result["eth"]["trade_in_use"]) + float(result["eth"]["withdrawal_in_use"]))
        wallet_status["etc"]["balance"] = str(float(result["etc"]["trade_in_use"]) + float(result["etc"]["withdrawal_in_use"]))
        wallet_status["bch"]["balance"] = str(float(result["bch"]["trade_in_use"]) + float(result["bch"]["withdrawal_in_use"]))
        wallet_status["xrp"]["balance"] = str(float(result["xrp"]["trade_in_use"]) + float(result["xrp"]["withdrawal_in_use"]))
        wallet_status["btg"]["balance"] = str(float(result["btg"]["trade_in_use"]) + float(result["btg"]["withdrawal_in_use"]))
        wallet_status["krw"]["balance"] = str(float(result["krw"]["trade_in_use"]) + float(result["krw"]["withdrawal_in_use"]))
        return wallet_status
        #return result["krw"]["available"]

    def get_list_my_orders(self, coin_type=None):
        """
        Get my order list
        """
        if coin_type is None:
            raise Exception("Need to coin_type")
        time.sleep(1)
        payload = {'currency_pair':coin_type}
        list_order_api_path = "/v1/user/orders/open"
        url_path = self.host + list_order_api_path
        headers = {"Authorization":"Bearer " + self.access_token}
        res = requests.get(url_path, headers=headers, params=payload)
        result = res.json()
        return result
    
    def get_my_order_status(self, coin_type=None, order_id=None):
        """
        get list my transaction history
        """
        if coin_type is None or order_id is None:
            raise Exception("Need to currency_pair and order id")
        time.sleep(1)
        list_transaction_api_path = "/v1/user/orders?currency_pair=" + coin_type 
        list_transaction_api_path += "&id=" + order_id
        url_path = self.host + list_transaction_api_path
        headers = {"Authorization":"Bearer " + self.access_token}
        res = requests.get(url_path, headers=headers)
        result = res.json()
        return result
  
    def get_orderbook(self, coin_type=None):
        if coin_type is None:
            raise Exception("Need to currency_pair")
        time.sleep(1)
        books_api_path = "/v1/orderbook?currency_pair="+coin_type
        url_path = self.host + books_api_path
        res = requests.get(url_path)
        result = res.json()
        return result
  
    def buy_coin_order(self, coin_type=None, price=None, qty=None, order_type="limit"):
        """
        buy_coin_order
        """
        time.sleep(1)
        if coin_type is None or price is None or qty is None:
            raise Exception("Need to param")
        buy_coin_order_api_path = "/v1/user/orders/buy"
        url_path = self.host + buy_coin_order_api_path
        headers = {"Authorization":"Bearer " + self.access_token}
        data = {"currency_pair":coin_type,
                "type":order_type,
                "price":price,
                "coin_amount":qty,
                "nonce":self.get_nonce()}
        res = requests.post(url_path, headers=headers, data=data)
        print("buy request:",res)
        result = res.json()
        return result

    def sell_coin_order(self, coin_type=None, price=None, qty=None, order_type="limit"):
        """
        buy_coin_order
        """
        time.sleep(1)
        if price is None or qty is None or coin_type is None:
            raise Exception("Need to params")
        if order_type != "limit":
            raise Exception("Check order type")
        buy_coin_order_api_path = "/v1/user/orders/sell"
        url_path = self.host + buy_coin_order_api_path
        headers = {"Authorization":"Bearer " + self.access_token}
        data = {"currency_pair":coin_type,
                "type":order_type,
                "price":price,
                "coin_amount":qty,
                "nonce":self.get_nonce()}
        res = requests.post(url_path, headers=headers, data=data)
        print("sell request:",res)
        result = res.json()
        return result

    def cancel_coin_order(self, coin_type=None, price=None, qty=None, order_type=None, order_id=None):
        """
        cancel_coin_order
        """
        time.sleep(1)
        if coin_type is None or order_id is None:
            raise Exception("Need to params")
        cancel_coin_order_api_path = "/v1/user/orders/cancel"
        url_path = self.host + cancel_coin_order_api_path
        headers = {"Authorization":"Bearer " + self.access_token}
        data = {"currency_pair":coin_type,
                "id":order_id,
                "nonce":self.get_nonce()}
        res = requests.post(url_path, headers=headers, data=data)
        result = res.json()
        return result

    def __repr__(self):
        return "(Korbit %s)"%self.username

    def __str__(self):
        return str("Korbit")
